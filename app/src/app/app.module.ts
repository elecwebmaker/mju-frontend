
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CoreModule } from './core/index';
import { SharedModule } from './shared/shared.module';
import { UtilityModule } from './utility/utility.module';
import { HttpModule,Http,XHRBackend,RequestOptions } from '@angular/http';
import { CustomHttpProvider } from './shared';
import { AppRoutingModule } from './app.routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    AppRoutingModule,
  ],
  providers: [
     { provide: Http,
        useFactory: CustomHttpProvider,
        deps: [XHRBackend, RequestOptions]
      },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }