import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { UserService } from './../../shared';
@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.scss']
})
export class AdminViewComponent implements OnInit {

  constructor(private routesnap:ActivatedRoute,private userservice:UserService,private router:Router) {
    
  }

  ngOnInit() {
    this.userservice.loginView(this.routesnap.snapshot.params["code"]).subscribe(()=>{
      this.router.navigate(['/member']);
    });
    
  }

}
