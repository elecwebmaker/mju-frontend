import { ContentLoadingService } from './content-loading.service';
export class ContentComp{
    loading:boolean = false;
    private contentloadingservice;
    constructor(contentloadingservice:ContentLoadingService){
        this.contentloadingservice = contentloadingservice;
    }

    initLoading():void{
        this.loading = true;
        this.contentloadingservice.setLoading(this.loading);
    }

    LoadingSuccess():void{
        this.loading = false;
        this.contentloadingservice.setLoading(this.loading );
    }
}