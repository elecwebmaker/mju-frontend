import { Component, OnInit,ContentChild,AfterViewInit } from '@angular/core';
import { FinanceDataService } from './../../shared';

import { ContentLoadingService } from './../content-loading.service';

import { SingleListComp } from './../single-list-comp';
declare var $:any;
@Component({
    selector: 'content-container',
    templateUrl: './content-container.comp.html',
    styleUrls:['./content-container.comp.scss'],
    providers:[FinanceDataService]
})
export class ContentContainer implements OnInit,AfterViewInit {
    loading:boolean;
    constructor(private contentloadingservice:ContentLoadingService) {

    }

    ngOnInit() { 
        this.contentloadingservice.getLoadingSubject().subscribe((res)=>{

            this.loading = res;
        });
       
    }
    ngAfterViewInit(){
        
    }
  
}