import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/Rx';
@Injectable()
export class ContentLoadingService {
    private loading:boolean = false;
    private loadingSubject:ReplaySubject<boolean> = new ReplaySubject(1);
    constructor() {
        
    }

    getLoadingSubject():ReplaySubject<boolean>{
        return this.loadingSubject;
    }

    setLoading(loading:boolean):void{
        this.loading = loading;
        this.loadingSubject.next(this.loading);
    }
}