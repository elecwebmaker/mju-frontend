import { NgModule } from '@angular/core';

import { TableComp } from './table-comp/table.comp';
import { TabularComp } from './tabular-comp';
import { SingleListComp } from './single-list-comp';
import { ContentContainer } from './content-container-comp';
import { MainContainerComp } from './main-container-comp';
import { WrapperFullComp } from './wrapper-full-comp';
import { WrapperSingleComp } from './wrapper-single-comp';
import { SharedModule } from './../shared/shared.module';
import { UtilityModule } from './../utility/utility.module';
import { TableHeadService } from './table-head/table-head.service';
import { CoreRouting } from './core.routing';
import { SizinationComp } from './sizination-comp/sizination-comp';
import { PaginationComp } from './pagination-comp/pagination-comp';
import { SubHeaderComp } from './sub-header-comp';
import { SingleListService } from './single-list/single-list.service';
import { RecieptComp } from './reciept-comp';
import { RecieptService } from './reciept.service';
import { ContentLoadingService } from './content-loading.service';
import { TableFootService } from './table-foot';
import { DivinedTabularComp } from './divined-tabular-comp';
import { DivinedTableComp } from './divined-table-comp';
import { LoanContactTabularComp } from './loancontact-tabular-comp';
import { GuaranterModalComp } from './guaranter-modal-comp';
import { GuaranterTabularComp } from './guaranter-tabular-comp';
import { ResetPassComp } from './reset-pass-comp';
import { ResetpassForm } from './resetpass-form-comp';
import { ResetPassService } from './reset-pass.service';
import { ResetPassPopup } from './resetpass-popup-comp';
import { AdminViewComponent } from './admin-view';
import { GuarantingTabularComponent } from './guaranting-tabular/guaranting-tabular.component';

import { LoanTabularCompComponent } from './loan-tabular-comp/loan-tabular-comp.component';
import { StockSingleComponent } from './stock-single/stock-single.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { LoanContractPageComponent } from './loan-contract-page/loan-contract-page.component';
import { StatusloanMemberComponent } from './statusloan-member/statusloan-member.component';
@NgModule({
    imports: [SharedModule,UtilityModule,CoreRouting],
    exports: [TableComp,ContentContainer,MainContainerComp,TabularComp,WrapperFullComp,CoreRouting,SizinationComp,PaginationComp,SingleListComp,WrapperSingleComp,SubHeaderComp,RecieptComp,DivinedTabularComp,DivinedTableComp,LoanContactTabularComp,GuaranterModalComp,GuaranterTabularComp,ResetPassComp,ResetpassForm,AdminViewComponent,ResetPassPopup],
    declarations: [TableComp,ContentContainer,MainContainerComp,TabularComp,WrapperFullComp,SizinationComp,PaginationComp,SingleListComp,WrapperSingleComp,SubHeaderComp,RecieptComp,DivinedTabularComp,DivinedTableComp,LoanContactTabularComp,GuaranterModalComp,GuaranterTabularComp,ResetPassComp,ResetpassForm, AdminViewComponent, GuarantingTabularComponent,ResetPassPopup, LoanTabularCompComponent, StockSingleComponent,ProfileComponent, ProfileViewComponent, LoanContractPageComponent, StatusloanMemberComponent],
    providers: [TableHeadService,SingleListService,RecieptService,ContentLoadingService,TableFootService, ResetPassService],
})
export class CoreModule { }
