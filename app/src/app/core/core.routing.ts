import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WrapperFullComp } from './wrapper-full-comp'
import { WrapperSingleComp } from './wrapper-single-comp'
import { TabularComp } from './tabular-comp';
import { SingleListComp } from './single-list-comp';
import { OpaqueToken } from '@angular/core';
import { LoginComp,AuthGuard,SingleDataArrayService,SingleDataMemberService,FinanceConfig } from './../shared';
import page from './../shared/page';
import { DivinedTabularComp } from './divined-tabular-comp';
import { RecieptComp } from './reciept-comp';
import { LoanContactTabularComp } from './loancontact-tabular-comp';
import { ResetPassComp } from './reset-pass-comp';
import { AdminViewComponent } from './admin-view';
import { GuarantingTabularComponent } from "app/core/guaranting-tabular/guaranting-tabular.component";
import { ProfileComponent } from "app/core/profile/profile.component";

export const deposite = new OpaqueToken('deposite');
export const divined = new OpaqueToken('devined');
export const loan = new OpaqueToken('loan');
export const stock = new OpaqueToken('stock');
export const member = new OpaqueToken('member'); 
export const loancontact = new OpaqueToken('loancontact'); 
export const guaran = new OpaqueToken('guaran');
import { LoanTabularCompComponent } from './loan-tabular-comp/loan-tabular-comp.component';
import { StockSingleComponent } from "./stock-single/stock-single.component";
import { LoanContractPageComponent } from "app/core/loan-contract-page/loan-contract-page.component";
export const mjuRoutes:Routes = [
    {
        path:'finance',
        component:WrapperFullComp,
        canActivate:[AuthGuard],
        children:[
            {
                path:'stock',
                component:StockSingleComponent,
                resolve:{
                    singledata:SingleDataMemberService,
                    resolveconfig:stock
                }
            },
            {
                path:'deposite',
                component:TabularComp,
                resolve:{
                    financeconfig:deposite
                }
            },
            {
                path:'divined',
                component:DivinedTabularComp,
                resolve:{
                    financeconfig:divined
                }
            },
            {
                path:'loan',
                component:LoanTabularCompComponent,
                resolve:{
                    financeconfig:loan
                }
            },
            {
                path:'loancontact',
                component:LoanContractPageComponent,
                resolve:{
                    financeconfig:loancontact
                }
            },
            {
                path:'guaran',
                component:GuarantingTabularComponent,
                resolve:{
                    financeconfig:guaran
                }
            },
            {
                path:'receipt',
                component:RecieptComp,
            },
            {
                path:'',
                redirectTo:'/finance/stock',
                pathMatch:"full"
            },
            
        ]
    },
    {
        path:"member",
        component:WrapperFullComp,
        canActivate:[AuthGuard],
        children:[
            {
                path:'',
                component: ProfileComponent,
                resolve:{
                    singledata:SingleDataMemberService,
                    resolveconfig:member
                }
            },
            {
                path:'resetpass',
                component:ResetPassComp,
            }
        ]
    },
    {
        path:"login",
        component:LoginComp
    },
    {
        path:'adminview/:code',
        component:AdminViewComponent
    },
    {
        path:'',
        redirectTo:'/login',
        pathMatch:"full"
    },
];


export function getDeposte(){
    return FinanceConfig.get(page.DEPOSITE);
}

export function getDevined(){
    return FinanceConfig.get(page.DEVINED);
}

export function getStock(){
    return FinanceConfig.get(page.STOCK);
}

export function getLoan(){
    return FinanceConfig.get(page.LOAN);
}

export function getMemberDetail(){
    return FinanceConfig.get(page.MEMBERDETAIL);
}

export function getLoanContact(){
    return FinanceConfig.get(page.LOANCONTACT);
}

export function getGuaran(){
    return FinanceConfig.get(page.Guaran);
}
@NgModule({
  imports: [
    RouterModule.forChild(mjuRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers:[
      {
        provide:deposite,
        useValue:getDeposte
    },
    {
        provide:divined,
        useValue:getDevined
    },
    {
        provide:stock,
        useValue:getStock
    },
    {
        provide:loan,
        useValue:getLoan
    },
    {
        provide:member,
        useValue:getMemberDetail
    },
    {
        provide:loancontact,
        useValue:getLoanContact
    },
    {
        provide:guaran,
        useValue:getGuaran
    },
  ]
})


export class CoreRouting { }