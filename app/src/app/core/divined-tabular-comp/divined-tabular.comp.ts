import { Component, OnInit } from '@angular/core';
import { TabularComp } from './../tabular-comp';
import { CFColumn, CFRow } from 'craft-table/craft-table';
import { CFTableControl } from "craft-table/craft-table";
import { IDataFinance } from "app/shared";
@Component({
    moduleId: module.id,
    selector: 'divined-tabular',
    templateUrl: './divined-tabular.comp.html',
    styleUrls:['./divined-tabular.comp.scss']
})

export class DivinedTabularComp extends TabularComp {
    cftable:CFTableControl =  new CFTableControl({
          navigating:false,
          offset:0,
          size:3
    });
    onClickColumn(cfitem:{column:CFColumn,row:CFRow}){
        if(cfitem.column.getId() == "year"){
                
        }

    }

    protected setGetData():void{
        this.cftable.setGetData((offset:number,size:number)=>{
                return this.financedataservice.getList(offset,size,{is_all_year:1}).map(this.mapData).map((res:IDataFinance,index)=>{
                    this.cftable.setDataTotal(res.total);
                    this.setFooterBody(res.total_data);
                    return res.data;
                });
        });
    }

    openFile(cfitem:{column:CFColumn,row:CFRow}){
        let url = '/devined/export';
        let col_id:CFColumn = cfitem.row.get('year'); 
        this.initLoading();
        this.http.post(url,{mcode:this.userservice.getUser().getCode(),year:col_id.getValue()}).subscribe((res:any)=>{
            this.LoadingSuccess();
            window.open(res.file);
        });
    }
}