import { Component, OnInit,ViewChild } from '@angular/core';
import { SemanticModalComponent } from 'ng-semantic/ng-semantic';
import { ModalNgs } from './../../shared';
import { GuaranterTabularComp } from './../guaranter-tabular-comp';
import { ContentLoadingService } from './../content-loading.service';
@Component({
    moduleId: module.id,
    selector: 'guaranter-modal',
    templateUrl: './guaranter-modal.comp.html',
    styleUrls:['./guaranter-modal.comp.scss']
})

export class GuaranterModalComp extends ModalNgs implements OnInit {
    @ViewChild(GuaranterTabularComp) guatabular:GuaranterTabularComp;
    showGua(guacode:string):void{
        console.log(guacode,"guacode");
        this.show();
        this.guatabular.setGuaCode(guacode);
        
    }
    constructor(private contentloadingservice:ContentLoadingService){
        super();
        // this.contentloadingservice.getLoadingSubject().subscribe((res)=>{
        //     if(!res){
        //         this.show();
        //     }
        // });
    }
}