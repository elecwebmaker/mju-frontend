import { Component, OnInit,Input } from '@angular/core';
import { TabularComp} from './../tabular-comp';
import { IDataFinance,FinanceDataService } from './../../shared';
import { genCFFootBody, CFTableControl } from 'craft-table/craft-table';
@Component({
    moduleId: module.id,
    selector: 'guaranter-tabular',
    templateUrl: './guaranter-tabular.comp.html',
    styleUrls:['./guaranter-tabular.comp.scss'],
    providers:[
        {
            provide:FinanceDataService,
            useClass:FinanceDataService
        }
    ]
})

export class GuaranterTabularComp extends TabularComp{
    @Input('guacode') guacode:string;
    isSetData:boolean = false;
    setDataInit:boolean = false;
    
    cftable:CFTableControl =  new CFTableControl({
          navigating:false,
       
        });
    setGuaCode(guacode:string){
        console.log(guacode,"guacode--table");
         this.guacode = guacode;
         this.financedataservice.setUrlAPi('/loan/getGuarantor/contract_no/'+guacode);

         if(!this.isSetData){
            this.setGetData();
            this.isSetData = true;
         }else{
              this.cftable.refresh();
         }
         
       
         
    }

    protected setupApi():void{
        this.financedataservice.setResProp('gua');
    }

    protected setGetData():void{
        this.cftable.setGetData((offset:number,size:number)=>{
                return this.financedataservice.getList(0,null).map((res:IDataFinance)=>{
                   
                    res.data = res.data.map((data,index)=>{ 
                        data["index"] = index+1;
                        data["gua_nameAndLast"] = data["member_name"] + " " + data["member_surname"];
                        return data;
                    });
                
                    this.cftable.setDataTotal(res.data.length);
                    return res.data;
                });
                
        });
    }

    protected setHeader(){
        
        this.cftable.setHeader(this.tableheadservice.get('guaranter'));
    }
    
}