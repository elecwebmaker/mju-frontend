import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuarantingTabularComponent } from './guaranting-tabular.component';

describe('GuarantingTabularComponent', () => {
  let component: GuarantingTabularComponent;
  let fixture: ComponentFixture<GuarantingTabularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuarantingTabularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuarantingTabularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
