import { Component, OnInit } from '@angular/core';
import { TabularComp } from "./../tabular-comp";
import { IDataFinance } from "app/shared";
import { parseMoney } from "app/utility";

@Component({
  selector: 'app-guaranting-tabular',
  templateUrl: './../tabular-comp/tabular.comp.html',
  styleUrls: ['./../tabular-comp/tabular.comp.scss']
})
export class GuarantingTabularComponent extends TabularComp{
    protected setupApi():void{
        this.financedataservice.setUrlAPi(this.route.snapshot.data['financeconfig'].url + this.userservice.getUser().getCode());
        this.financedataservice.setResProp(this.route.snapshot.data['financeconfig'].resprop);
    }
    
    protected mapData(res:IDataFinance){
        res.data = res.data.map((item:any,index)=>{
          item["gua_nameAndLast"] = item["member_name"] + " " + item["member_surname"];
          item["amount"] = parseMoney(item["amount"]);
          return item;
        });
        return res;
    }

     protected setGetData():void{
        this.cftable.setGetData((offset:number,size:number)=>{
                return this.financedataservice.getList(offset,size).map(this.mapData).map((res:IDataFinance,index)=>{
                    this.cftable.setDataTotal(res.data.length);
                    this.setFooterBody(res.total_data);
                    return res.data;
                });
        });
    }
}
