import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanContractPageComponent } from './loan-contract-page.component';

describe('LoanContractPageComponent', () => {
  let component: LoanContractPageComponent;
  let fixture: ComponentFixture<LoanContractPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanContractPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanContractPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
