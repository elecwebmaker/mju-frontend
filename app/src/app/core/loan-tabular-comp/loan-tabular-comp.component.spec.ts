import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanTabularCompComponent } from './loan-tabular-comp.component';

describe('LoanTabularCompComponent', () => {
  let component: LoanTabularCompComponent;
  let fixture: ComponentFixture<LoanTabularCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanTabularCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanTabularCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
