import { Component, OnInit, ViewChild } from '@angular/core';
import { TabularComp } from './../tabular-comp';
import { CFColumn } from "craft-table/craft-table";
import { CFRow } from "craft-table/craft-table";
import { GuaranterModalComp } from "./../guaranter-modal-comp";
@Component({
  selector: 'app-loan-tabular-comp',
  templateUrl: './loan-tabular-comp.component.html',
  styleUrls: ['./../tabular-comp/tabular.comp.scss']
})
export class LoanTabularCompComponent extends TabularComp implements OnInit {
     @ViewChild(GuaranterModalComp) guamodal:GuaranterModalComp;
     onClickColumn(cfitem:{column:CFColumn,row:CFRow}){
          this.guamodal.showGua(cfitem.row.get('contract_no').getValue());
    }
}
