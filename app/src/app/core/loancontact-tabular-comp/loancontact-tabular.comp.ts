import { Component, OnInit,ViewChild } from '@angular/core';
import { TabularComp } from './../tabular-comp';
import { CFColumn, CFRow } from 'craft-table/craft-table';
import { IDataFinance } from './../../shared';
import { GuaranterModalComp } from './../guaranter-modal-comp';
import { getLoanMemberStatus, getLoanContract } from "app/utility";
@Component({
    moduleId: module.id,
    selector: 'loancontact-tabular',
    templateUrl: './../tabular-comp/tabular.comp.html',
    styleUrls:['./../tabular-comp/tabular.comp.scss']
})

export class LoanContactTabularComp extends TabularComp {
    // @ViewChild(GuaranterModalComp) guamodal:GuaranterModalComp;
    // onClickColumn(cfitem:{column:CFColumn,row:CFRow}){
    //     if(cfitem.column.getId() == "doc_no"){
    //         this.guamodal.showGua(cfitem.column.getValue());
            
    //     }
    // }

    protected mapData(res:IDataFinance){
        res.data = res.data.map((data)=>{
             data["status"] = getLoanContract(data["status"]);
             return data;
        });
        return res;
    }
}