import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'main-container',
    templateUrl: './main-container.comp.html',
    styleUrls:['./main-container.comp.scss']
})
export class MainContainerComp implements OnInit {
    constructor() { }

    ngOnInit() { }
}