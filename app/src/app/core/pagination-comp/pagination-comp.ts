import { Component, OnInit,forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CFPaginationComp } from 'craft-table/craft-table';
export const CUSTOM_SIZE_CONTROL_VALUE_ACCESSOR:any = {
    provide:NG_VALUE_ACCESSOR,
    useExisting:forwardRef(()=>PaginationComp),
    multi:true
}
@Component({
    selector: 'pagination-comp',
    templateUrl: './pagination-comp.html',
    styleUrls:['./pagination-comp.scss'],
    providers:[CUSTOM_SIZE_CONTROL_VALUE_ACCESSOR]
})
export class PaginationComp extends CFPaginationComp implements OnInit {
    constructor() {
        super();
     }

    ngOnInit() { }
}