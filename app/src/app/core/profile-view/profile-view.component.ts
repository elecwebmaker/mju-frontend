import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {
  @Input('data') profile:object;
  constructor() { }

  ngOnInit() {
    
  }

}
