import { Component, OnInit } from '@angular/core';
import { SingleListComp } from "./../single-list-comp";
import { statusMember } from "app/utility";
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends SingleListComp {
  public member;
  public address;
  public img_path;
  public status;
  public profile:object = {};
  protected onResSuccess(){
    this.member = this.listview.getHead("ข้อมูลสมาชิก");
    this.address = this.listview.getHead("ข้อมูลที่อยู่");
    this.img_path = this.member.get("รูป");
    this.status = statusMember(this.member.get('สถานภาพ'));
    // this.profile = {
    //   code:this.member.get("เลขทะเบียน"),
    //   name:this.member.get("ชื่อ"),
    //   surname:this.member.get("นามสกุล"),
    //   citizen_id:this.member.get("เลขประจำตัวประชาชน"),
    //   age_range:this.member.get("อายุสมาชิก"),
    //   age_start:this.member.get("")
    // }
  }
  
}
