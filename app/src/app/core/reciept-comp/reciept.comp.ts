import { Component, OnInit } from '@angular/core';
import { RecieptService } from './../reciept.service';
import { ContentComp } from './../content-comp';
import { ContentLoadingService } from './../content-loading.service';
import { genNumberToThaiMonth, CfMonth } from './../../utility';
import { Observable,Subject } from "rxjs/Rx";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/core';

export interface ReceiptItem{
    month:string;
    year:string;
    monthObj?:CfMonth,
    loading:boolean,
    isEmpty:boolean
};
@Component({
    moduleId: module.id,
    selector: 'reciept-comp',
    templateUrl: './reciept.comp.html',
    styleUrls:['./reciept.comp.scss'],
    animations:[
        trigger('noDataState', [
        state('inactive', style({
            // display:'none',
            opacity:'0',
            transform:'translateX(100px)'
        })),
        state('active',   style({
            display:'inline-block',
            opacity:'1',
            transform:'translateX(0px)'
        })),
        transition('inactive => active', animate('50ms ease-in')),
        transition('active => inactive', animate('50ms ease-out'))
        ])
    ]
})
export class RecieptComp extends ContentComp implements OnInit {
    receiptLists:Array<ReceiptItem> = [
    ];
    noDataTrigger:string = 'inactive';
    constructor(private recieptservice:RecieptService,contentloadingservice:ContentLoadingService) {
        super(contentloadingservice);
    }

    ngOnInit() {
        this.initLoading();
        this.recieptservice.getData().subscribe((res:Array<ReceiptItem>)=>{
            this.LoadingSuccess();
            this.receiptLists = res.map((item)=>{
                item.loading = false;
                item.isEmpty = false;
                item.monthObj = genNumberToThaiMonth(item.month);
                return item;
            });
        });

    }

    openFile(item:ReceiptItem):void{
        
        item.loading = true;
        this.recieptservice.getReciept(item.month,item.year).subscribe((res:any)=>{
             if(res.receipt){
                window.open(res.receipt);
             }else{
                item.isEmpty = true;
             
                window.setTimeout(()=>{
                    item.isEmpty = false;
                    
                },4000)
             }
             
             item.loading = false;
        })
       
    }
}