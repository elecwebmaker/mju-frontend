import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ReceiptItem } from './reciept-comp';
import { UserService } from './../shared';
@Injectable()
export class RecieptService {
    url:string = "/receipt/list";
    res_prop:string = "list";
    constructor(private http:Http,private userservice:UserService) {

    }

    getData():Observable<Array<object>>{
        return this.http.post(this.url,{}).map((res)=>{
            res[this.res_prop].push({month:11,year:2558});
            return res[this.res_prop];
        });
    }

    getReciept(month:string,year:string):Observable<object>{
        let url = "/receipt/generate/mcode/" + this.userservice.getUser().getCode();
        let body = {
            "month":month,
            "year":year
        };
        return this.http.post(url,body).map((res)=>{
            return res;
        });
    }
}