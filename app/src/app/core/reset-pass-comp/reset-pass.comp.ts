import { Component, OnInit, ViewChild } from '@angular/core';
import { ResetPassService } from "../reset-pass.service";
import { ResetPassPopup } from "./../resetpass-popup-comp";
import { Observable } from "rxjs/Rx";

@Component({
    moduleId: module.id,
    selector: 'resetpass',
    templateUrl: './reset-pass.comp.html',
    styleUrls:['./reset-pass.comp.scss']
})

export class ResetPassComp implements OnInit {
    @ViewChild(ResetPassPopup) popup: ResetPassPopup
    constructor(private resetpassservice:ResetPassService) {

    }

    ngOnInit() { 
     
    }

    changePass(evt:{oldpass:string,newpass:string}){
        this.resetpassservice.changePass(evt.oldpass,evt.newpass).subscribe((res)=>{
            console.log("erer");
            this.popup.showRes(true);
        },(err)=>{
            console.log(err);
            this.popup.showRes(false);
            return Observable.throw(err);
        });
    }
}