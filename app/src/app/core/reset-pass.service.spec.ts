import { TestBed, inject } from '@angular/core/testing';

import { ResetPassService } from './reset-pass.service';

describe('ResetPassService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResetPassService]
    });
  });

  it('should ...', inject([ResetPassService], (service: ResetPassService) => {
    expect(service).toBeTruthy();
  }));
});
