import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UserService } from "app/shared";
@Injectable()
export class ResetPassService {

  constructor(private http:Http,private userservice:UserService) { 
    
  }

  changePass(oldpass:string,newpass:string){
    let url = "/member/resetPassword";
    let body = {
      op:oldpass,
      np:newpass,
      mcode:this.userservice.getUser().getCode()
    };
    return this.http.post(url,body);
  }
}
