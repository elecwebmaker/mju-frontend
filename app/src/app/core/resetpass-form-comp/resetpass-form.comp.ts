import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { validateConfirmPassword } from "app/utility";
@Component({
    moduleId: module.id,
    selector: 'resetpass-form',
    templateUrl: './resetpass-form.comp.html',
    styleUrls:['./resetpass-form.comp.scss']
})

export class ResetpassForm implements OnInit {
    public setPassForm: FormGroup;
    public submitted: Boolean = false;
    constructor(private _fb: FormBuilder) { }
    @Output('onsubmit') onsubmit:EventEmitter<{oldpass:string,newpass:string}> = new EventEmitter<{oldpass:string,newpass:string}>();
    ngOnInit() {
        this.setPassForm = this._fb.group({
            curPass: ['',Validators.required],
            newPass: [''],
            renewPass: ['']
        });
        this.setPassForm.get('renewPass').setValidators([<any>Validators.required, validateConfirmPassword(<FormControl>this.setPassForm.get('newPass'))]);
        this.setPassForm.get('newPass').setValidators([<any>Validators.required, validateConfirmPassword(<FormControl>this.setPassForm.get('renewPass'),true)]);
    }

    submit():void {
        this.submitted = true;
        if(this.setPassForm.valid){
            this.onsubmit.emit({
                oldpass: this.setPassForm.get('curPass').value,
                newpass: this.setPassForm.get('newPass').value
            });
        }
    }
}