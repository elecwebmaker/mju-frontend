import { Component, OnInit, ViewChild } from '@angular/core';
import { SemanticModalComponent } from 'ng-semantic/ng-semantic';
import { ModalNgs } from './../../shared';

@Component({
    moduleId: module.id,
    selector: 'popup-modal',
    templateUrl: './resetpass-popup.comp.html'
})

export class ResetPassPopup extends ModalNgs implements OnInit {
    public message: string;
    constructor() {
        super();
    }
    showRes(p: Boolean) {
        console.log(p);
        p? this.message = 'เปลี่ยนรหัสผ่านสำเร็จ': this.message = 'กรุณาใส่รหัสผ่านให้ถูกต้อง';
        this.show();
    }
    ngOnInit() { }
}