import { Component, OnInit } from '@angular/core';
import  { ListView } from './../../shared/list-view';
import { FinanceDataService,IDataFinance } from './../../shared';
import {ActivatedRoute} from '@angular/router';
import { SingleListService } from './../single-list';
import { ResolveDataAbstract } from './../../shared';
import { ContentComp } from './../content-comp';
import { ContentLoadingService } from './../content-loading.service';
@Component({
    selector: 'single-list-comp',
    templateUrl: './single-list.comp.html',
    styleUrls: ['./single-list.comp.scss'],
})
export class SingleListComp extends ContentComp implements OnInit {
    listview:ListView;
    constructor(private route: ActivatedRoute,private singlelistservice:SingleListService,contentloadingservice:ContentLoadingService) { 
        super(contentloadingservice);
        this.listview = this.singlelistservice.get(this.route.snapshot.data['resolveconfig'].page);
    }

    ngOnInit() { 
       let resolveService = <ResolveDataAbstract>this.route.snapshot.data['singledata'];
       resolveService.setUrl(this.route.snapshot.data['resolveconfig'].url);
       resolveService.setResProp(this.route.snapshot.data['resolveconfig'].resprop);
       this.initLoading();

       resolveService.getData().map(this.mapData).subscribe((res)=>{
             this.LoadingSuccess();
             this.listview.setData(res);
             this.onResSuccess();
            
       })
      
       this.init();
      
        
    }
    protected init(){

    }
    protected onResSuccess(){

    }
    protected mapData(res):any{
        console.log("res",res);
        return res;
    }
}