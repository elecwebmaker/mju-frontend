import { Injectable } from '@angular/core';
import { ListviewItem,ListView } from './../../shared/list-view';
import page from './../../shared/page';
@Injectable()
export class SingleListService {
    private tableMap:Object = {};
    constructor() {
        let member_detail = new ListView("member");
        member_detail.setHeader("ข้อมูลสมาชิก",[
                new ListviewItem("member_code","เลขทะเบียน"),
                new ListviewItem("member_name","ชื่อ"),
                new ListviewItem("member_surname","นามสกุล"),
                new ListviewItem("citizen_id","เลขประจำตัวประชาชน"),
                new ListviewItem("member_age","อายุสมาชิก"),
                new ListviewItem("start_date","เริ่มเป็นสมาชิก"),
                new ListviewItem("birth_day","วัน/เดือน/ปีเกิด"),
                new ListviewItem("open_card","วันออกบัตร"),
                new ListviewItem("expired_card","วันหมดอายุ"),
                new ListviewItem("status","สถานภาพ"),
                new ListviewItem("img_path","รูป"),
        ]);

        member_detail.setHeader("ข้อมูลที่อยู่",[
            new ListviewItem("address","ที่อยู่"),
            new ListviewItem("province","จังหวัด"),
            new ListviewItem("zipcode","รหัสไปรษณีย์"),
            new ListviewItem("agent_name","หน่วยงาน"),
            new ListviewItem("mobile_tel","โทรศัพท์"),
            new ListviewItem("internal_tel","โทรศัพท์ภายใน"),
        ])
        let stock = new ListView();
        stock.setHeader(null,[new ListviewItem("share_update","ยอดคงเหลือ")])
        this.set(page.STOCK,stock);
        this.set(page.MEMBERDETAIL,member_detail);

    }

    get(prop_name:page):ListView{
        return this.tableMap[prop_name];
    }

    set(prop_name:page,listviewitems:ListView){
        this.tableMap[prop_name] = listviewitems;
    }
}