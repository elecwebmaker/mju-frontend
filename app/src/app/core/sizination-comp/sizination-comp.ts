import { Component, OnInit, forwardRef,AfterViewInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CFSizinationComp } from 'craft-table/craft-table';
export const CUSTOM_SIZE_CONTROL_VALUE_ACCESSOR:any = {
    provide:NG_VALUE_ACCESSOR,
    useExisting:forwardRef(()=>SizinationComp),
    multi:true
}
declare var $: any;
@Component({
    selector: 'sizination-comp',
    templateUrl: './sizination-comp.html',
    styleUrls:['./sizination-comp.scss'],
    providers:[CUSTOM_SIZE_CONTROL_VALUE_ACCESSOR]
})
export class SizinationComp extends CFSizinationComp implements OnInit,AfterViewInit{
    constructor() { 
        super();
    }

    ngOnInit() { 
        
    }

    ngAfterViewInit(){
    //    $('.ui.dropdown').dropdown('reset');
     
        //console.log($('.ui.dropdown').dropdown,"dropdown");
    }
}