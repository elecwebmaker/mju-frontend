import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusloanMemberComponent } from './statusloan-member.component';

describe('StatusloanMemberComponent', () => {
  let component: StatusloanMemberComponent;
  let fixture: ComponentFixture<StatusloanMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusloanMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusloanMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
