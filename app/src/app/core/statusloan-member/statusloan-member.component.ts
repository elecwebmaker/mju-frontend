import { Component, OnInit } from '@angular/core';
import { SingleDataMemberService } from "app/shared";
import { getLoanMemberNoti } from './../../utility';
@Component({
  selector: 'app-statusloan-member',
  templateUrl: './statusloan-member.component.html',
  styleUrls: ['./statusloan-member.component.scss']
})
export class StatusloanMemberComponent implements OnInit {
  status:string;
  constructor(private sgdatamemservice:SingleDataMemberService) {
    this.sgdatamemservice.getData().subscribe((res)=>{
      this.status = getLoanMemberNoti(res["request_status"]);
     
    })
  }

  ngOnInit() {
  }

}
