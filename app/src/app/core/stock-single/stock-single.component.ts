import { Component, OnInit } from '@angular/core';
import { SingleListComp } from "app/core/single-list-comp";
@Component({
  selector: 'app-stock-single',
  templateUrl: './../single-list-comp/single-list.comp.html',
  styleUrls: ['./../single-list-comp/single-list.comp.scss']
})
export class StockSingleComponent extends SingleListComp implements OnInit {

  mapData(res){
    console.log(res,"res");
    res.share_balance = res.share_balance + " บาท";
    return res;
  }

}
