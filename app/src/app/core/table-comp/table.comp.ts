import { Component, OnInit ,Input, EventEmitter,Output,TemplateRef,ElementRef} from '@angular/core';
import { CFTableControl,Ihead,Type } from 'craft-table/craft-table';
import { CFColumn, CFRow } from 'craft-table/craft-table'

@Component({
    selector: 'table-comp',
    templateUrl: './table.comp.html',
    styleUrls:['./table.comp.scss']
})
export class TableComp implements OnInit {
     @Input ('class') class:string;
    @Input('control') cftable:CFTableControl;
    @Input('extendColumn') cextendcolumn:TemplateRef<ElementRef>;
    @Output() onClickColumn:EventEmitter<{column:CFColumn,row:CFRow}> = new EventEmitter<{column:CFColumn,row:CFRow}>();

    constructor() {
        // this.cftable =  new CFTableControl({
        //   header:[],
        //   navigating:true,
        // });
     }

    ngOnInit() { 
    //   console.log("hedr",this.cftable.getTable().header.heads)
}

    onClickCol(column:CFColumn,row:CFRow){
        this.onClickColumn.emit({column:column,row:row});
    }
}