import { CFHead,Type,Ihead,genCFHeader} from 'craft-table/craft-table';

export const deposite = genCFHeader(<Array<Ihead>>[
   {
       id:"acc_balance",
       type:Type.TEXT,
       pos:4
   },
    {
       id:"int_balance",
       type:Type.TEXT,
       pos:5
   }
]);

export const loan = genCFHeader(<Array<Ihead>>[
    {
      id:"allow_balance",
      type:Type.TEXT,
      pos:4
    },
    {
      id:"paid_qty",
      type:Type.TEXT,
      pos:7
    },
    //   {
    //   id:"paid",
    //   type:Type.TEXT,
    //   pos:8
    // },
    {
      id:"balance",
      type:Type.TEXT,
      pos:8
    },
]);

export const divined =  genCFHeader(<Array<Ihead>>[
    // {
    //   id:"payback_rate",
    //   type:Type.TEXT,
    //   label:"อัตราเงินปันผล",
    //   pos:2
    // },
    // {
    //   id:"payback_rate_avg",
    //   type:Type.TEXT,
    //   label:"อัตราเงินเฉลี่ยคืน",
    //   pos:3
    // },
    // {
    //   id:"balance",
    //   type:Type.TEXT,
    //   label:"ทุนเรือนหุ้น",
    //   pos:4
    // },
    // {
    //   id:"interested",
    //   type:Type.TEXT,
    //   label:"ดอกเบี้ยสะสม",
    //   pos:5
    // },
    // {
    //   id:"payback",
    //   type:Type.TEXT,
    //   label:"เงินปันผล",
    //   pos:6
    // },
    // {
    //   id:"payback_avg",
    //   type:Type.TEXT,
    //   label:"เงินปันผลเฉลี่ยคืน",
    //   pos:7
    // },
    // {
    //   id:"payback_total",
    //   type:Type.TEXT,
    //   label:"เงินปันผลเฉลี่ยคืน",
    //   pos:8
    // },
]);
