import { TableHeadService } from './../table-head';
import page from './../../shared/page';
import * as tableheadpage from './table-foot-page';

export class TableFootService extends TableHeadService{
    constructor(){
        super();
    }
    init(){
         this.set(page.DEVINED,tableheadpage.divined);

        this.set(page.DEPOSITE,tableheadpage.deposite);

        this.set(page.LOAN,tableheadpage.loan);
        // this.set(page.LOANCONTACT,tableheadpage.loan);
    }
}