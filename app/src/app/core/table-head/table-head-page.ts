import { CFHead,Type,Ihead,genCFHeader} from 'craft-table/craft-table';

// export const deposite = [
//     new CFHead(Type.TEXT,"acc_type_name","ประเภทเงินฝาก"),
//     new CFHead(Type.TEXT,"acc_id","เลขบัญชี"),
//     new CFHead(Type.TEXT,"acc_name","ชื่อบัญชี"),
//     new CFHead(Type.TEXT,"acc_balance","ยอดคงเหลือ"),
//     new CFHead(Type.TEXT,"int_balance","ดอกเบี้ยสะสม"),
// ];

export const deposite = genCFHeader(<Array<Ihead>>[
    {
      id:"acc_type_name",
      type:Type.TEXT,
      label:"ประเภทเงินฝาก",
    },
    {
      id:"acc_id",
      type:Type.TEXT,
      label:"เลขบัญชี"
    },
    {
      id:"acc_name",
      type:Type.TEXT,
      label:"ชื่อบัญชี"
    },
    {
      id:"acc_balance",
      type:Type.TEXT,
      label:"ยอดคงเหลือ"
    },
    {
      id:"int_balance",
      type:Type.TEXT,
      label:"ดอกเบี้ยสะสม"
    },
]);

export const loan = genCFHeader(<Array<Ihead>>[
    {
      id:"@index",
      type:Type.TEXT,
      label:"ลำดับ"
    },
    {
      id:"contract_no",
      type:Type.TEXT,
      label:"เลขที่สัญญา"
    },
    {
      id:"allow_date",
      type:Type.TEXT,
      label:"วันที่อนุมัตเงินกู้"
    },
    {
      id:"allow_balance",
      type:Type.TEXT,
      label:"วงเงินอนุมัติ"
    },
    {
      id:"type",
      type:Type.TEXT,
      label:"ประเภท"
    },
    {
      id:"paid_type",
      type:Type.TEXT,
      label:"แบบชำระ"
    },
    {
      id:"paid_qty",
      type:Type.TEXT,
      label:"ชำระงวดล่ะ"
    },
    // {
    //   id:"paid",
    //   type:Type.TEXT,
    //   label:"ชำระ"
    // },
    {
      id:"balance",
      type:Type.TEXT,
      label:"คงเหลือ"
    },
]);

// export const divined = [
//     new CFHead(Type.TEXT,"payback_rate","อัตราเงินปันผล"), // not known
//     new CFHead(Type.TEXT,"payback_rate_avg","อัตราเงินเฉลี่ยคืน"), // not known
//     new CFHead(Type.TEXT,"balance","ทุนเรือนหุ้น"),
//     new CFHead(Type.TEXT,"interested","ดอกเบี้ยสะสม"),
//     new CFHead(Type.TEXT,"payback","เงินปันผล"),
//     new CFHead(Type.TEXT,"payback_avg","เงินปันผลเฉลี่ยคืน"),
// ];
export const divined =  genCFHeader(<Array<Ihead>>[
  {
    id:"year",
    type:Type.TEXT,
    label:"ปี",
    style:{
        // ["text-decoration"]:"underline",
        // cursor:"pointer",
        // color:"rgba(97, 169, 7, 0.83)"
      }
  },
    {
      id:"payback_rate",
      type:Type.TEXT,
      label:"อัตราเงินปันผล"
    },
    {
      id:"payback_rate_avg",
      type:Type.TEXT,
      label:"อัตราเงินเฉลี่ยคืน"
    },
    {
      id:"balance",
      type:Type.TEXT,
      label:"ทุนเรือนหุ้น"
    },
    {
      id:"payback",
      type:Type.TEXT,
      label:"เงินปันผล"
    },
    {
      id:"interested",
      type:Type.TEXT,
      label:"ดอกเบี้ยเงินกู้"
    },

    {
      id:"payback_avg",
      type:Type.TEXT,
      label:"เงินเฉลี่ยคืน"
    },
    {
      id:"payback_total",
      type:Type.TEXT,
      label:"รวมปันผลเฉลี่ยคืน"
    },
]);

export const loancontact =  genCFHeader(<Array<Ihead>>[
    {
      id:"status",
      type:Type.TEXT,
      label:"สถานะ"
    },
    {
      id:"doc_no",
      type:Type.TEXT,
      label:"เลขที่สัญญา"
      // style:{
      //   ["text-decoration"]:"underline",
      //   cursor:"pointer",
      //   color:"rgba(97, 169, 7, 0.83)"
      // }
    },
    {
      id:"allow_balance",
      type:Type.TEXT,
      label:"วงเงินกู้"
    },
    {
      id:"type",
      type:Type.TEXT,
      label:"ประเภทชำระ"
    },
    {
      id:"paid_amount",
      type:Type.TEXT,
      label:"จำนวนชำระ"
    }
   
]);


export const guaranter = genCFHeader(<Array<Ihead>>[
    {
      id:"@index",
      type:Type.TEXT,
      label:"ลำดับ",
    },
    {
      id:"gua_nameAndLast",
      type:Type.TEXT,
      label:"ชื่อ-สกุล"
    },   
]);

export const guaranpage = genCFHeader(<Array<Ihead>>[
    {
      id:"@index",
      type:Type.TEXT,
      label:"ลำดับ",
    },
    {
      id:"contract_no",
      type:Type.TEXT,
      label:"เลขที่สัญญา"
    },  
    {
      id:"gua_nameAndLast",
      type:Type.TEXT,
      label:"ชื่อผู้กู้"
    },  
    {
      id:"agent_name",
      type:Type.TEXT,
      label:"หน่วยงาน"
    },  
    {
      id:"amount",
      type:Type.TEXT,
      label:"จำนวนเงินค้ำประกัน"
    },  
]);
