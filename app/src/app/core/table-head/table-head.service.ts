import { Injectable } from '@angular/core';
import { CFHeader} from 'craft-table/craft-table';
import page from './../../shared/page';
import * as tableheadpage from './table-head-page';
@Injectable()
export class TableHeadService {
    private tableMap:Object = {};
    constructor() { 
        this.init();
    }

    get(prop_name:page | string):CFHeader{
        return this.tableMap[prop_name];
    }

    set(prop_name:page | string,header:CFHeader){
        this.tableMap[prop_name] = header;
    }

    init(){
        this.set(page.DEVINED,tableheadpage.divined);

        this.set(page.DEPOSITE,tableheadpage.deposite);

        this.set(page.LOAN,tableheadpage.loan);
        
        this.set(page.LOANCONTACT,tableheadpage.loancontact);

        this.set('guaranter',tableheadpage.guaranter);

        this.set(page.Guaran,tableheadpage.guaranpage);
    }
}