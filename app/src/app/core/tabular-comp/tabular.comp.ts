import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { FinanceDataService,IDataFinance } from './../../shared';
import { TableComp } from './../table-comp';
import { TableHeadService } from './../table-head/table-head.service';
import { TableFootService } from './../table-foot';
import page from './../../shared/page';
import {ActivatedRoute} from '@angular/router';
import { ContentComp } from './../content-comp';
import { CFTableControl,Ihead,Type,genCFFootBody } from 'craft-table/craft-table';
import { ContentLoadingService } from './../content-loading.service';
import { CFColumn, CFRow } from 'craft-table/craft-table';
import { Http } from '@angular/http';
import { UserService } from './../../shared';
@Component({
    selector: 'tabular-comp',
    templateUrl: './tabular.comp.html',
    styleUrls:['./tabular.comp.scss']
})
export class TabularComp extends ContentComp implements OnInit,AfterViewInit {
   
    cftable:CFTableControl =  new CFTableControl({
          navigating:true,
    });
    currentPage:number = 1;
    currentSize:number = 10;
    setDataInit:boolean = true;
    constructor(protected userservice:UserService,protected http:Http,protected tablefootservice:TableFootService,protected financedataservice:FinanceDataService,protected tableheadservice:TableHeadService,protected route: ActivatedRoute,contentloadingservice:ContentLoadingService) { 
        super(contentloadingservice);
    }

    ngOnInit() { 
        this.setUp();
        
    }

    protected setUp(){
       
        this.initTable();
        this.cftable.setFooterHeader(this.tablefootservice.get(this.route.snapshot.data['financeconfig'].page));
    }

    ngAfterViewInit(){
        
    }

    private initTable(){
            this.setupApi();
            if(this.setDataInit){
                 this.setGetData();
            }
           
            this.setHeader();
    }

    protected setupApi():void{
        this.financedataservice.setUrlAPi(this.route.snapshot.data['financeconfig'].url);
        this.financedataservice.setResProp(this.route.snapshot.data['financeconfig'].resprop);
    }
    
    protected setGetData():void{
        this.cftable.setGetData((offset:number,size:number)=>{
                return this.financedataservice.getList(offset,size).map(this.mapData).map((res:IDataFinance,index)=>{
                    this.cftable.setDataTotal(res.total);
                    this.setFooterBody(res.total_data);
                    return res.data;
                });
        });
    }
    protected setHeader(){
        this.cftable.setHeader(this.tableheadservice.get(this.route.snapshot.data['financeconfig'].page));
    }

    onClickColumn(cfitem:{column:CFColumn,row:CFRow}){
        
    }

    protected setFooterBody(data){
         this.cftable.setFooterBody(genCFFootBody(data));
    }

    protected mapData(res:IDataFinance){
        return res;
    }
}