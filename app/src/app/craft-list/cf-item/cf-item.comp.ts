import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'cf-item',
    templateUrl: './cf-item.comp.html',
    styleUrls:['./cf-item.comp.scss']
})
export class CraftItemComp implements OnInit {
    constructor() { }

    ngOnInit() { }
}