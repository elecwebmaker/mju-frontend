import { Component, OnInit,Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'cf-list',
    templateUrl: './cf-list.comp.html',
    styleUrls:['./cf-list.comp.scss']
})
export class CraftListComp implements OnInit {
    constructor() { }

    ngOnInit() { }
    test(){
        window.alert("it works");
    }

    @Input('greet') greet;
}