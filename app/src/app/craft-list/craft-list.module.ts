import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CraftListComp } from './cf-list';
import { CraftItemComp } from './cf-item';

@NgModule({
    imports: [CommonModule],
    exports: [CraftListComp,CraftItemComp],
    declarations: [CraftListComp,CraftItemComp],
    providers: [],
})
export class CraftListModule { }
