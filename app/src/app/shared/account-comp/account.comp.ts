import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Router } from '@angular/router';
import { genDateTimeToObject,ObjDateTime } from './../../utility';
@Component({
    selector: 'account-comp',
    templateUrl: './account.comp.html',
    styleUrls:['./account.comp.scss']
})
export class AccountComp implements OnInit {
    public name:string;
    public lastTime:ObjDateTime;
    public code:string;
    protected datework:string;
    public lastTimeString:string;
    constructor(private userservice:UserService,private router:Router) { 
        this.code = this.userservice.getUser().getCode();
        this.name = this.userservice.getUser().getName();
        this.lastTime = genDateTimeToObject(this.userservice.getUser().getLastTimeUsed());
        if(this.lastTime){
            this.lastTimeString = this.lastTime.d +" "+ this.lastTime.m.short + " " + this.lastTime.year + "  " + this.lastTime.h + ":" + this.lastTime.mi;  
        }
        this.datework = this.userservice.getUser().getDateWork();
    }
        
    ngOnInit() {
        
    }

    logout(){
        this.userservice.logout();
        this.router.navigate(['/login']);
    }
} 