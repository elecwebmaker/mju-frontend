import { Injectable } from '@angular/core';
import { API_URL } from './API';
import { ConnectionBackend,RequestOptions,Http,Request,RequestOptionsArgs,Response,XHRBackend,Headers,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
export class CustomHttp extends Http{
    baseUrl:string;
    constructor(private backend: ConnectionBackend, private defaultOptions: RequestOptions){
        super(backend,defaultOptions);
        this.baseUrl = API_URL;
    }

    // request(url:Request, options?: RequestOptionsArgs): Observable<Response> {
    //     // let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    //     // let optionse = new RequestOptions({ headers: headers });
    //     return super.request(url.url,options).catch(err => {
    //         return err;
    //     });
    // }
    

    post(url: string, body: any, absolute_url:boolean = false) {
        let params = new URLSearchParams();
        for(let item in body){
            params.append(item,body[item]);
        }
        let url_api:string = "";
        if(absolute_url){
            url_api = url;
        }else{
            url_api = this.baseUrl + url;
        }
        return super.post(url_api,params,new RequestOptions()).map((res:Response)=>{
           return res.json(); 
        }).do((res)=>{
            
        }).catch(err=>{
            
            return Observable.throw(err.json());
        });
    }

    private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
        }
        return options;
    }
}

export function CustomHttpProvider(backend: XHRBackend, defaultOptions: RequestOptions){ 
    return new CustomHttp(backend, defaultOptions);
}
