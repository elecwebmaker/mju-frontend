export class DataService{
    private list:Array<Object>;
    getList():Array<Object>{
        return this.list;
    }
    setList(list:Array<Object>):void{
        this.list = list;
    }
}