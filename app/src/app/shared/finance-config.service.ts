import { Injectable } from '@angular/core';
import page from './page';
export interface IFinanceConfig{
    url:string,
    resprop:string,
    page:page
};

export module FinanceConfig {
    var financeconfigs:Object = {};

    export const get = (prop_name:page):IFinanceConfig=>{
        return financeconfigs[prop_name];
    }

    export const set = (prop_name:page,config:IFinanceConfig)=>{
        financeconfigs[prop_name] = config;
    }

    
};

FinanceConfig.set(page.MEMBERDETAIL,{
    url:"/member/get/mcode",
    resprop:"member",
    page:page.MEMBERDETAIL
});

FinanceConfig.set(page.DEPOSITE,{
    url:"/deposite/list",
    resprop:"deposit",
    page:page.DEPOSITE
});

FinanceConfig.set(page.STOCK,{
    url:"/member/get/mcode",
    resprop:"member",
    page:page.STOCK
});

FinanceConfig.set(page.DEVINED,{
    url:"/devined/list",
    resprop:"devined",
    page:page.DEVINED
});

FinanceConfig.set(page.LOAN,{
    url:"/loan/list",
    resprop:"loans",
    page:page.LOAN
});

FinanceConfig.set(page.LOANCONTACT,{
    url:"/contract/list",
    resprop:"contracts",
    page:page.LOANCONTACT
});

FinanceConfig.set(page.Guaran,{
    url:"/contract/getGuarantorOther/mcode/",
    resprop:"gua",
    page:page.Guaran
});