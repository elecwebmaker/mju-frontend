import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { Http } from '@angular/http';
import { Observable} from 'rxjs/Rx';
import { UserService } from './user.service';
export interface IDataFinance{
    data:Array<Object>
    total:number,
    total_data:Object
}
@Injectable()

export class FinanceDataService{
    private url_api:string;
    private res_prop:string;
    private dataservice:DataService = new DataService();
    private isAbsolute:boolean = false;
    constructor(private http:Http,private userservice:UserService) {

    }

    getUrlApi():string{
        
        return this.url_api;
    }

    setUrlAPi(url_api:string,isAbsolute:boolean = false):void{
        this.isAbsolute = isAbsolute;
        this.url_api = url_api;
    }

    setResProp(res_prop:string):void{
        this.res_prop = res_prop;
    }

    getResProp():string{
        return this.res_prop;
    }

    getList(offset:number,size:number,filter:Object = {}):Observable<IDataFinance>{
        let body = {
            offset:offset,
            size:size
        };
        filter = Object.assign({},{
            key:this.userservice.getUser().getCode()
        },filter)
        body = Object.assign({},body,filter);
        return this.http.post(this.url_api,body,this.isAbsolute).map((res:object)=>{
            this.dataservice.setList(res[this.res_prop]);
            return {data:res[this.res_prop],total:res["count"],total_data:res["total"]};  
        });
    }
}