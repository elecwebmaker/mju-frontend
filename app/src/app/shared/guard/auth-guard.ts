import { Injectable } from '@angular/core';
import { CanActivate,Router } from '@angular/router';
import { UserService } from './../user.service';
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private userservice:UserService,private router:Router){

    }
    canActivate() {
        if(this.userservice.isLogged()){
            return true;
        }else{
            this.router.navigate(['/login']);
            return false;
        }
    }
}