import { ListviewItem } from "./listview-item";
import { ListView } from "./list-view";

export class ListHead {
    lists:Array<ListviewItem> = [];
    constructor(lists:Array<ListviewItem>,private listview:ListView){
        this.lists = lists;
    }

    get(label:string){
        let item_name  = this.lists.filter((item)=>{
            return item.label == label;
        })[0].name;
        return this.listview.getDataItem(item_name);
    }
}