import { ListviewItem } from './listview-item';
import { ListHead } from "./list-head";
export class ListView{
    headerlists:Object = {};
    data:Object = {};

    constructor(head?:string){
        if(!head){
            this.addHeader("$implicit");
        }else{
            this.addHeader(head);
        }
        
    }

    setData(data:Object):void{
        this.data = data;
    }

    getDataItem(name:string):string | number{

        return this.data[name];
    }

    addHeader(head:string):void{
        this.headerlists[head];
    }

    setHeader(head:string,value:Array<ListviewItem>):void{
        if(!head){
            this.headerlists["$implicit"] = value;
        }else{
            this.headerlists[head] = value;
        }
        
    }

    getHeader():Array<string>{
        let headerArray = [];
        for(let prop in this.headerlists){
            headerArray.push(prop);
        }
        
        return headerArray;
    };

    getList(head:string):Array<ListviewItem>{
        return this.headerlists[head];
    }

    getHead(head:string){

        return new ListHead(this.getList(head),this);
    }
}