import { Component, OnInit,AfterViewInit } from '@angular/core';
import { UserService } from './../../shared/user.service';
import { Observable } from 'rxjs/Rx';
import { Router,ActivatedRoute } from '@angular/router';
export interface User{
    username:any,
    password:any,
    isSave:boolean
}
declare var $:any;
@Component({
    selector: 'login-comp',
    templateUrl: './login.comp.html',
    styleUrls:['./login.comp.scss']
})


export class LoginComp implements OnInit,AfterViewInit {
    user:User;
    invalid:boolean;
    loading:boolean;
    err_message:string;
    constructor(private userservice:UserService,private router: Router,private route: ActivatedRoute) {
        
    }

    ngOnInit() { 
        this.user = {
            username:"",
            password:"",
            isSave:false
        };
        if(this.userservice.isLogged()){
            this.router.navigate(['/member'])
        }
    }

    login(model:User,isValid:boolean,isSave:boolean):boolean{
        if(!isValid){
            return false;
        }
        this.loading = true;
        this.userservice.login(model.username,model.password,isSave).map(res=>{
            this.loading = false;
            return res;
        }).catch((err)=>{
            if(err.code == "1001" || err.code == "1002"){
                this.invalid = true;
                this.err_message = err.message;
            }

            return Observable.empty();
        }).subscribe((res)=>{
            this.router.navigate(['/member'])
        });
    }

    ngAfterViewInit(){
        $('.checkbox').checkbox();
    }
}