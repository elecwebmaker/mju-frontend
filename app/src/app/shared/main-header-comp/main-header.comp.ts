import { Component, OnInit, Input } from '@angular/core';
import { MenuToggleService } from './../menu-toggle.service';
@Component({
    selector: 'main-header',
    templateUrl: './main-header.comp.html',
    styleUrls:['./main-header.comp.scss']
})
export class MainHeaderComp implements OnInit {
    @Input() hasMenu:boolean = true;
    title:string = "สหกรณ์ออมทรัพย์มหาวิทยาลัยแม่โจ้จำกัด";
    redirect:string = "/member";
    isOpen:boolean;
    constructor(private menutoggleservice:MenuToggleService) {

    }

    ngOnInit() { 
        this.menutoggleservice.openEvtSubject().subscribe((res)=>{
            this.isOpen = res;
        });
    }

    toggle(){
        this.menutoggleservice.toggle();
    }
}