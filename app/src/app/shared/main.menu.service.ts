import { Injectable } from '@angular/core';
import { MenuItem } from './menu-item/menu-item';
@Injectable()
export class MainMenuService {
    menuItems:Array<MenuItem> = [];
    constructor() { 
      this.init();
    }

    getMenuItems():Array<MenuItem>{
        return this.menuItems;
    }

    setMenuItems(menuItems:Array<MenuItem>):void{
        this.menuItems = menuItems;
    }

    add(menu:MenuItem):void{
        this.menuItems.push(menu);
    }

    init(){
        this.add(new MenuItem("หน้าแรก","/member"));
        this.add(new MenuItem("ทุนเรือนหุ้น","/finance/stock"));
        this.add(new MenuItem("เงินฝาก","/finance/deposite"));       
        this.add(new MenuItem("ปันผล/เฉลี่ยคืน","/finance/divined"));
        this.add(new MenuItem("เงินกู้","/finance/loan"));
        this.add(new MenuItem("พิมพ์ใบเสร็จ","/finance/receipt"));
        this.add(new MenuItem("ภาระค้ำประกัน","/finance/guaran"));
        this.add(new MenuItem("สถานะการยื่นกู้","/finance/loancontact"));
        // this.add(new MenuItem("ข้อมูลใบเสร็จรับเงิน",""));
    }
}