import { Component, OnInit } from '@angular/core';
import { MenuItem } from './../menu-item';
import { MainMenuService } from './../main.menu.service';
import { Route,Router } from '@angular/router';
import { MenuToggleService } from './../menu-toggle.service';
@Component({
    selector: 'main-menu-comp',
    templateUrl: './menu.comp.html',
    styleUrls: ['./menu.comp.scss']
})
export class MenuComp implements OnInit {
    menuItems:Array<MenuItem> = [];
    isOpen:boolean;
    constructor(private mainmenuservice:MainMenuService,private router:Router,private menutoggleservice:MenuToggleService) { 
        this.menuItems = this.mainmenuservice.getMenuItems();
    }

    ngOnInit() {
        this.menutoggleservice.openEvtSubject().subscribe((res)=>{
            this.isOpen = res;
        });
    }

    redirectTo(url:string){
        this.router.navigate([url]);
    }
}