import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/Rx';
@Injectable()
export class MenuToggleService {
    private isOpen:boolean = false;
    private openEvt:ReplaySubject<boolean> =  new ReplaySubject<Boolean>(1);
    constructor() { 

    }

    openEvtSubject():ReplaySubject<boolean>{
        return this.openEvt;
    }
    
    toggle(){
        this.isOpen = !this.isOpen;
        this.openEvt.next(this.isOpen);
    }
}