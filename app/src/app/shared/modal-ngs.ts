import { Component, OnInit,ViewChild } from '@angular/core';
import { SemanticModalComponent } from 'ng-semantic/ng-semantic';
@Component({
    moduleId: module.id,
    template:""
})

export class ModalNgs implements OnInit {
    @ViewChild(SemanticModalComponent) smmodal:SemanticModalComponent;

    ngOnInit() {

    }
    
    show():void{
        this.smmodal.show();
    }

    hide():void{
        this.smmodal.hide();
    }

}