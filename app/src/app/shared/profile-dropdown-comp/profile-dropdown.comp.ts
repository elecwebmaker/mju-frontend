import { Component, OnInit,AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './../user.service';
declare var $:any;
@Component({
    moduleId: module.id,
    selector: 'profile-dropdown',
    templateUrl: './profile-dropdown.comp.html',
    styleUrls:['./profile-dropdown.comp.scss']
})

export class ProfileDropdown implements OnInit,AfterViewInit {
    constructor(private userservice:UserService,private router:Router) { }

    ngOnInit() { 

    }

    logout(){
        this.userservice.logout();
        this.router.navigate(['/login']);
    }

    ngAfterViewInit(){
        $('.ui.dropdown').dropdown({
            on: 'hover'
        });
    }
}