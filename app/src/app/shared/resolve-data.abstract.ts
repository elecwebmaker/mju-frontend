import { Observable } from 'rxjs/Rx';
import { Resolve } from '@angular/router';
export abstract class ResolveDataAbstract implements Resolve<ResolveDataAbstract> {
    url:string;
    res_prop:string;
    abstract getData():Observable<Object | Array<Object>>;
    setUrl(url:string):void{
        this.url = url;
    }
    setResProp(resprop:string):void{
        this.res_prop = resprop;
    }
    resolve(){
        return this;
    }
}