
import { User } from "./user";

export var PropStorage = {
    ACCESSTOKEN:"mjuf",
    USERNAME:"mjuf-username",
    CODE:"mjuf-code",
    LASTTIMEUSED:"mjuf-last_timeused",
    DATEWORK:"mjuf-date_work"
}

export class SessionUser{

    constructor(private user:User){
        
    }

    saveToLocal():void{
        window.localStorage.setItem(PropStorage.ACCESSTOKEN, this.user.getAccessToken());
        window.localStorage.setItem(PropStorage.USERNAME, this.user.getName());
        window.localStorage.setItem(PropStorage.CODE, this.user.getCode());
        window.localStorage.setItem(PropStorage.LASTTIMEUSED, this.user.getLastTimeUsed());
        window.localStorage.setItem(PropStorage.DATEWORK, this.user.getDateWork());
    }

    saveToSession():void{
        window.sessionStorage.setItem(PropStorage.ACCESSTOKEN, this.user.getAccessToken());
        window.sessionStorage.setItem(PropStorage.USERNAME, this.user.getName());
        window.sessionStorage.setItem(PropStorage.CODE, this.user.getCode());
        window.sessionStorage.setItem(PropStorage.LASTTIMEUSED, this.user.getLastTimeUsed());        
        window.sessionStorage.setItem(PropStorage.DATEWORK, this.user.getDateWork());
    }

    get(proname:string){
        return window.sessionStorage.getItem(proname) || window.localStorage.getItem(proname)
    }

    removeLocal():void{
        for(let prop in PropStorage){
            window.localStorage.removeItem(PropStorage[prop]);
        }
    }

    removeSession():void{
        for(let prop in PropStorage){
            window.sessionStorage.removeItem(PropStorage[prop]);
        }
    }
}