import { NgModule } from '@angular/core';
import { AccountComp } from './account-comp/index';
import { MenuComp } from './menu-comp/index';
import { MainHeaderComp } from './main-header-comp';
import { UtilityModule } from './../utility/utility.module';
import { UserService } from './user.service';
import { MainMenuService } from './main.menu.service';
import { SingleDataMemberService } from './single-data-member.service';
import { SingleDataArrayService } from './single-data-array.service';
import { LoginComp } from './login-comp';
import { AuthGuard } from './guard/auth-guard';
import { MenuToggleService } from './menu-toggle.service';
import { ProfileDropdown } from './profile-dropdown-comp';
import { TwoSideContent } from './twoside-comp';
import { NgSemanticModule } from 'ng-semantic/ng-semantic';
import { ForgetpassComp } from './forgetpass-comp';
import { SubMenuComp } from './sub-menu-comp';
import { SubMenuService } from './sub-menu.service';
import { WrapperComp } from './wrapper-comp';
import { ModalNgs } from './modal-ngs';
import { WrapperCardComponent } from './wrapper-card/wrapper-card.component';
@NgModule({
    imports: [UtilityModule,NgSemanticModule],
    exports: [AccountComp,MainHeaderComp,MenuComp,ProfileDropdown,TwoSideContent,ForgetpassComp,SubMenuComp,NgSemanticModule,WrapperComp,ModalNgs, WrapperCardComponent],
    declarations: [AccountComp,MainHeaderComp,MenuComp,LoginComp,ProfileDropdown,TwoSideContent,ForgetpassComp,SubMenuComp,WrapperComp,ModalNgs, WrapperCardComponent],
    providers: [
        UserService,
        MainMenuService,
        SingleDataMemberService,
        SingleDataArrayService,
        AuthGuard,
        MenuToggleService,
        SubMenuService
    ],
})
export class SharedModule { 
    
}
