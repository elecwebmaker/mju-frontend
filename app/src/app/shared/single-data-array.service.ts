import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ResolveDataAbstract } from './resolve-data.abstract';
@Injectable()
export class SingleDataArrayService extends ResolveDataAbstract  {
    constructor(private http:Http) { 
        super();
    }


    getData():Observable<Array<Object>>{
        return this.http.post(this.url,{offset:0,size:1}).map((res:any)=>{
            return res[this.res_prop][0];
        })
    }

   
}