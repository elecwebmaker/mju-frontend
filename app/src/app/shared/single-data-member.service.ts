import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UserService } from './user.service';
import { Observable } from 'rxjs/Rx';
import { ResolveDataAbstract } from './resolve-data.abstract';
import { parseMoney } from "app/utility";
@Injectable()
export class SingleDataMemberService extends ResolveDataAbstract  {
    constructor(private http:Http,private userservice:UserService) { 
        super();
    }


    getData():Observable<Object>{
        return this.http.post("/member/get/mcode/"+this.userservice.getUser().getCode(),{}).map((res:any)=>{
            return res["member"];
        }).map((res)=>{
            res.member_age = res.member_age_year +  " ปี " + res.member_age_month + " เดือน";
            res["share_update"] = parseMoney(res["share_update"]) + " บาท";
            return res; 
        })
    }

   
}