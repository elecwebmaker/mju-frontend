import { Component, OnInit } from '@angular/core';
import { MenuComp } from './../menu-comp';
import { MainMenuService } from './../main.menu.service';
import { SubMenuService } from './../sub-menu.service';
@Component({
    moduleId: module.id,
    selector: 'sub-menu',
    templateUrl: './sub-menu.comp.html',
    styleUrls:['./sub-menu.comp.scss'],
    providers:[
        {
            provide:MainMenuService,
            useClass:SubMenuService
        }
    ]
})

export class SubMenuComp extends MenuComp implements OnInit {
    menuBgList:Array<{image, text}> = [];
    
    ngOnInit() { 
        let menuBg = ["half_bt_receipt.png","half_bt_dividend.png","half_bt_loancontract.png"];
        let menuText = ["Receipt", "Dividned", "Loan Contract"];
        this.menuBgList = menuBg.map((item,index)=>{
            let path = "assets/icon";
            item = "assets/icon/"+item;
            return {image: item, text: menuText[index]};
        });
    }

    
}