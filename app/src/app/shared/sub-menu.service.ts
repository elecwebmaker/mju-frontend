import { MainMenuService } from './main.menu.service';
import { MenuItem } from './menu-item/menu-item';
export class SubMenuService extends MainMenuService{
    init(){
        this.add(new MenuItem("พิมพ์ใบเสร็จ","/finance/receipt"));
        this.add(new MenuItem("ปันผล/เฉลี่ยคืน","/finance/divined"));       
        this.add(new MenuItem("สถานะการยื่นกู้","/finance/loancontact"));
    }
}