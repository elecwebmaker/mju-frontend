import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'two-side',
    templateUrl: './twoside.comp.html',
    styleUrls: ['./twoside.comp.scss']
})

export class TwoSideContent implements OnInit {
    constructor() { }

    ngOnInit() { }
}