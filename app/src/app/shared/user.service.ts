import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable } from 'rxjs/Rx';
import { Http } from '@angular/http';
import { SessionUser,PropStorage } from './session.user';
declare var md5:any;

function insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
}
@Injectable()

export class UserService {
    private user:User = new User();
    private session:SessionUser = new SessionUser(this.user);
    constructor(private http: Http) { 

    }
    logged:boolean = false;
    getUser():User{
        return this.user;
    }

    saveUserDetail(username:string,code:string,last_timeused:string,datework:string){
       
      
        this.user.setCode(code);
        this.user.setName(username);
        this.user.setLastTimeUsed(last_timeused);
        this.user.setDateWork(datework);
       
    }
    saveTokenLocal(isSession){
        this.session.saveToSession();
        if(isSession){
             this.session.saveToLocal();
        }
    }

    loginView(code:string):Observable<object>{
        let url = "/member/info/mcode/"+code;
        return this.http.post(url,{}).map((res:any)=>{
            this.saveUserDetail(res.member_name,code,res.last_access,res.date);
            this.user.setAccessToken("mock-token");
            this.saveTokenLocal(false);
            this.logged = true;
            return res;
        });
        
    }

    login(email, password,isSave) {
            if (!email || !password) {
                return Observable.throw({ code: 300, message: "please provide emal and password" });

            }
            let url = '/member/login';
            let hashpass = insert((md5(password)).toString(),4,"1");//.splice(4,0,"1")
            let body = {
                u: email,
                p: hashpass,
            };
            
            
            let post$ = this.http.post(url, body).map((res:any) => {
                
                if(res.success){
                    this.logged = true;
                    var response = {
                        username: res.member_name,
                        code: res.member_code,
                        access_token: "mocktoken",
                        last_timeused:res.last_access,
                        datework:res.date
                    };
                  
                    this.saveUserDetail(response.username,response.code,response.last_timeused,response.datework);
                    this.user.setAccessToken(response.access_token);
                    this.saveTokenLocal(isSave);
                    
                }
            }).catch((res)=>{
                let message = "";
                if(res.code == "1001"){
                    message = "ยูสเซอร์เนมหรือรหัสผ่านไม่ถูกต้อง";
                }else if(res.code == "1002"){
                    message = "เข้าผิดเกิน 3 ครัั้ง กรุณาติดต่อสหกรณ์ออมทรัพย์";
                }
                return Observable.throw({ code: res.code, message: message });
            });

            return post$;
        }

        logout() {
            this.logged = false;
            this.removeToken();
        }


        
        removeToken() {
           this.session.removeLocal();
           this.session.removeSession();
        }

        isLogged() {
            if (!this.logged) {
                return this.getTokenFromLocal() ? true : false;

            }
            return this.logged ? true : false;
        }

        getTokenFromLocal() {
            let token =  this.session.get(PropStorage.ACCESSTOKEN);
            let username = this.session.get(PropStorage.USERNAME);
            let code = this.session.get(PropStorage.CODE);
            let last_timeused = this.session.get(PropStorage.LASTTIMEUSED);
            let datework = this.session.get(PropStorage.DATEWORK);
            


            if (username) {
                 this.saveUserDetail(username,code,last_timeused,datework);
                 this.user.setAccessToken(token);
            }
            if (token) {
                this.logged = true;
            }
            return token;
        }
}