export class User{
    private name:string;
    private code:string;
    private detail:Object = {};
    private lastimeUsed:string = "";
    private accesstoken:string = "";
    private datework:string = "";
    constructor(){
       
    }
    setDateWork(datework:string):void{
        this.datework = datework;
    }
    getDateWork():string{
        return this.datework;
    }
    setName(name:string):void{
        this.name = name;
    }
    setCode(code:string):void{
        this.code = code;
    }
    setAccessToken(accesstoken:string):void{
        this.accesstoken = accesstoken;
    }

    getAccessToken():string{
        return this.accesstoken;
    }
    getCode():string{
        return this.code;
    }
    getName():string{
        return this.name;
    }

    setDetail(detail:Object):void{
        this.detail = detail;
    }

    getDetail():Object{
        return this.detail;
    }

    setDetailItem(prop:string,value:any):void{
        this.detail[prop] = value;
    }

    getDetailItem(prop:string):string{
        return this.detail[prop];
    }

    setLastTimeUsed(time:string):void{
        this.lastimeUsed = time;
    }

    getLastTimeUsed():string{
        return this.lastimeUsed;
    }
}