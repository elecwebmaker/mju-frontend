import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'wrapper',
    templateUrl: './wrapper.comp.html',
    styleUrls:['./wrapper.comp.scss']
})

export class WrapperComp implements OnInit {
    constructor() { }

    ngOnInit() { }
}