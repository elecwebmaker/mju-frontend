import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { CFTableModule } from 'craft-table/craft-table';
import { CraftListModule } from './../craft-list';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
    imports: [],
    exports: [ReactiveFormsModule,RouterModule,FormsModule,HttpModule,BrowserModule,CFTableModule,CraftListModule,BrowserAnimationsModule],
    declarations: [],
    providers: [],
})
export class UtilityModule { }
