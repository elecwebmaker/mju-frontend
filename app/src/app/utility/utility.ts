import {FormControl} from "@angular/forms";
export interface CfMonth{
    full:string,
    short:string
}

export interface ObjDateTime{
    m:CfMonth,//month
    d:string,
    year:string,
    h:string,
    mi:string//minute,
    s:string
}


export interface ObjDate{
    m:CfMonth,//month
    d:string,
    year:string,
}

export interface ObjTime{
    h:string,
    mi:string//minute,
    s:string
}


//month number based on 1 - 12
export function genNumberToThaiMonth(monthnumber:string):CfMonth{//input ex: 03
    let month = {
        1:{
            full: 'มกราคม',
            short: 'ม.ค.'
        },
        2:{
            full: 'กุมภาพันธ์',
            short: 'ก.พ.'
        },
        3:{
            full: 'มีนาคม',
            short: 'มี.ค.'
        },
        4:{
            full: 'เมษายน',
            short: 'เม.ย.'
        },
        5:{
            full: 'พฤษภาคม',
            short: 'พ.ค.'
        },
        6:{
            full: 'มิถุนายน',
            short: 'ม.ย.'
        },
        7:{
            full: 'กรกฏาคม',
            short: 'ก.ค.'
        },
        8:{
            full: 'สิงหาคม',
            short: 'ส.ค.'
        },
        9:{
            full: 'กันยายน',
            short: 'ก.ย.'
        },
        10:{
            full: 'ตุลาคม',
            short: 'ต.ค.'
        },
        11:{
            full: 'พฤศจิกายน',
            short: 'พ.ย.'
        },
        12:{
            full: 'ธันวาคม',
            short: 'ธ.ค.'
        }
    }
    return month[parseInt(monthnumber)];
}

export function genDateTimeToObject(datetime:string):ObjDateTime{//input ex 04:04:2558 11:40:00
    let output = datetime.split(' ');
    return {
        m: genDateToObject(output[0]).m,
        d: genDateToObject(output[0]).d,
        year: genDateToObject(output[0]).year,
        h: genTimeToObject(output[1]).h,
        mi: genTimeToObject(output[1]).mi,
        s: genTimeToObject(output[1]).s
    }
}

export function genDateToObject(datetime):ObjDate{//input ex 04:04:2558
    let output = datetime.split('/');
    return {
        m: genNumberToThaiMonth(output[1]),
        d: parseInt(output[0]) + "",
        year: output[2]
    };
}

export function genTimeToObject(datetime):ObjTime{//input ex 11:40:00
    let output = datetime.split(':');
    return {
        h: output[0],
        mi: output[1],
        s: output[2]
    }
}

export function validateConfirmPassword(oldC: FormControl,reverse:boolean = false){
    return  (c: FormControl) =>{
        if (c.value === oldC.value && reverse) {
            if(oldC.errors){
                delete oldC.errors['validateEqual'];
            }
            
            if (oldC.errors && !Object.keys(oldC.errors).length){
                oldC.setErrors(null);
            }
            return null;
        }

     
        if (c.value !== oldC.value && reverse) {
            oldC.setErrors({ validateEqual: false });
            return null;
        }

        if(!reverse){
            return (c.value == oldC.value)? null : {
                validateConfirmPassword: {
                    valid: false
                }
            }
        };
    }
}

export function statusMember(key:string){
    key = key+"";
    let mapData = {
        "4":"ปกติ",
        "A":"ลาออกรับค่าหุ้นแล้ว",
        "8":"ลาออกยังไม่ได้รับค่าหุ้น"
    }
    return mapData[key];
}

export function parseMoney(num:string | number):string{
   return Number(num).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

export function getLoanMemberStatus(status:string){
      if(status == "1"){
           return "รับเอกสาร";
        }else if(status == "2"){
           return "พิมพ์สัญญา";
        }else if(status == "3"){
           return "รอเซนต์สัญญา";
        }else if(status == "4"){
           return "รออนุมัติ";
        }else{
           return "อนุมัติ/จ่ายแล้ว";
        }
}

export function getLoanMemberNoti(status){
    if(status == "1" || status == "2" ){
        return getLoanMemberStatus(status);
    }else{
        return 
    }
}

export function getLoanContract(status){
    if(status == "3" || status == "4" || status == "5" ){
        return getLoanMemberStatus(status);
    }else{
        return 
    }
}