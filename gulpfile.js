var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var browserSync = require('browser-sync');


gulp.task('sass', function () {
    return sass("scss/master.scss",{compass:true,style:'compressed'}).on('error', sass.logError).pipe(gulp.dest('css')).pipe(browserSync.stream());
});
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});
gulp.task('default',['browser-sync'], function() {
    gulp.watch('scss/**/*.scss', ['sass']);
   // gulp.watch('css/**/*.css', browserSync.reload);
    gulp.watch('templates/*.html', browserSync.reload);
    gulp.watch('./*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
    gulp.watch('css/**/*.css', browserSync.reload);

});
