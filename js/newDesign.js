$('.ui.dropdown')
  .dropdown()
;

$(window).scroll(function(){
	if ($(this).scrollTop() > 20) {
		$('#mu_scrollTop').css('opacity','0.5');
		$('#mu_scrollTop').css('z-index','10');
	} else {
		$('#mu_scrollTop').css('opacity','0');
		$('#mu_scrollTop').css('z-index','-1');
	}
});

$('#mu_scrollTop').click(function(){
	$('body,html').animate({
		scrollTop: 0 
	}, 200);
});

function ToggleMenu() {
	var MenuBarToggle = $('.mu_MenuBarToggle');
	var menuBar = $('.mu_MenuBar');
	var body = $('body');
	var header = $('header');
	var scorllTop = $('#mu_scrollTop');
	if (MenuBarToggle.hasClass("active_B2C")) {
		body.css("left", "0");
		header.css("left", "0");
		menuBar.css("right", "-250px");
		scorllTop.css("right", "20px");
		menuBar.removeClass("menuBar_shadow");
		MenuBarToggle.removeClass("active_B2C");
	} else {
		body.css("left", "-250px");
		header.css("left", "-250px");
		menuBar.css("right", "0");
		scorllTop.css("right", "270px");
		menuBar.addClass("menuBar_shadow");
		MenuBarToggle.addClass("active_B2C");
	}
}